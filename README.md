# Vexomine

A voxel based game made in godot with high customizability

## License

This project is licensed under [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

